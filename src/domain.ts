import { GetKeyType, Key } from './key';
import { Provider, provide, isProvider } from './providers';
import { Service, assertServiceIsStamped, serviceKeyMap, StampedService } from './service';

/** Describes a domain with providers. */
export interface Domain {
    readonly providers: readonly Provider<Key<unknown>>[];
    readonly services?: readonly (Service | Provider<Key<Service>>)[];
}

/** Describes an internal representation of a domain. This uses a cache to speed up lookup times. */
export interface InternalDomain extends Domain {
    readonly $$: Map<Key<unknown>, Provider<Key<unknown>>>;
}

/** The current domain set for this execution context. */
export let currentDomain: InternalDomain | void;

/** Asserts this execution context is not inside a domain. */
export function assertNotInDomain (message?: string) {
    if (currentDomain) {
        throw new Error(message || `This code is already running inside a domain execution context.`);
    }
}

/** Asserts this execution contect is inside a domain. */
export function assertInDomain (message?: string) {
    if (!currentDomain) {
        throw new Error(message || 'This code needs to run inside a domain execution context.');
    }
}

/** Creates an internal representation of a `Domain`. */
function createInternalDomain (domain: Domain): InternalDomain {
    return {
        ...domain,
        $$: new Map<Key<unknown>, Provider<Key<unknown>>> (
            [
                ...domain.providers.map(provider => [provider.key, provider.provide])  as any,
                ...(domain.services?.map(serviceDescriptor => {
                    if (isProvider(serviceDescriptor)) {
                        assertServiceIsStamped(serviceDescriptor.provide);
                        return [serviceDescriptor.key, serviceDescriptor.provide];
                    } else {
                        assertServiceIsStamped(serviceDescriptor);
                        const key = serviceKeyMap.get(serviceDescriptor as StampedService);
                        return [key, serviceDescriptor];
                    }
                }) ?? [])
            ]
        )
    };
}

/** Runs a function inside the execution context of a domain. */
export function runInDomain (baseDomain: Domain, callback: (domain: Domain) => void) {
    let oldDomain = currentDomain;
    let domain: InternalDomain = currentDomain ? createInternalDomain({
        ...currentDomain,
        ...baseDomain,
        providers: [
            // Filter out old domain providers that provide for the same key as this new domain.
            // Any providers from this new domain must take precedence over the old providers.
            ...baseDomain.providers,
            ...currentDomain.providers.filter(provider => !baseDomain.providers.find(xProvider => xProvider.key === provider.key))
        ],
        services: [
            ...(baseDomain.services ?? []),
            ...(currentDomain.services?.filter(service => !(currentDomain as InternalDomain).services?.includes(service)) ?? [])
        ]
    }) : createInternalDomain(baseDomain);

    currentDomain = domain;
    callback(domain);
    currentDomain = oldDomain;
}

