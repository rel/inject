import { assertInDomain, currentDomain, Domain, InternalDomain } from './domain';
import { hasOwnProperty } from './utilities';

export const Nothing = Object.assign(Symbol('DI.Nothing'), { __nothing__: true });

export type Key<TValue> = symbol & { __value__: TValue };

export type GetKeyType<TKey> = TKey extends Key<infer T> ? T : never;

export function createKey<TValue> (name: string): Key<TValue> {
    return Symbol(`DI.Key(${name})`) as unknown as Key<TValue>;
}

export function useKey<TKey extends Key<any>> (key: TKey, defaultValue: GetKeyType<TKey> | typeof Nothing = Nothing): GetKeyType<TKey> {
    assertInDomain();
    const internalCurrentDomain = currentDomain as unknown as InternalDomain;
    const { $$ } = internalCurrentDomain;
    const value = $$.has(key) ? $$.get(key) : defaultValue;

    if (value === Nothing) {
        throw new Error(`The current domain does not have a provider for key "${String(key)}", and a default value has not been provided.`);
    }

    return value as GetKeyType<TKey>;
}
