import { Key, GetKeyType } from './key';
import { hasOwnProperty } from './utilities';

export interface Provider<TKey extends Key<unknown> = Key<unknown>> {
    key: TKey;
    provide: GetKeyType<TKey>;
}

export function provide<TKey extends Key<unknown>> (key: TKey, provide: GetKeyType<TKey>): Provider<TKey> {
    return { key, provide };
}

export function isProvider (thing: any): thing is Provider<Key<unknown>> {
    return typeof thing === 'object' && typeof thing.key === 'symbol' && hasOwnProperty(thing, 'provide');
}
