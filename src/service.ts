import { Key, createKey } from './key';
import { Provider } from './providers';
import { assertInDomain, currentDomain, InternalDomain } from './domain';

export type Service = Record<PropertyKey, unknown>;

export type StampedService = Service & {
    __is_a_service__: true
};

export const serviceKeyMap = new WeakMap<StampedService, Key<Service>>();

export function createService<TService extends Service>(service: TService): TService {
    assertServiceIsNotStamped(service);
    serviceKeyMap.set(service as StampedService, createKey<Service>('GenericServiceKey'));
    // For now...
    return service;
}

export function assertServiceIsStamped(service: Service, message?: string) {
    if (!serviceKeyMap.has(service as StampedService)) {
        throw new Error(message || 'Did you forget to wrap this service in `createService`?');
    }
}

export function assertServiceIsNotStamped(service: Service, message?: string) {
    if (serviceKeyMap.has(service as StampedService)) {
        throw new Error(message || `This service has already been registered!`);
    }
}

export function provideService<TService extends Service, TOverridingService extends TService>(service: TService, override: TOverridingService): Provider {
    assertServiceIsStamped(service);
    const key = serviceKeyMap.get(service as StampedService) as Key<TService>;

    return {
        key,
        provide: override
    };
}

export function useService<TService extends Service>(service: TService): TService {
    assertInDomain();
    assertServiceIsStamped(service);

    const domain = (currentDomain as unknown as InternalDomain);
    const key = serviceKeyMap.get(service as StampedService) as Key<unknown>;

    const override = (domain.$$.get(key)?.provide as TService);
    return override ?? service;
}
