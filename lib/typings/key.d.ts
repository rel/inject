export declare const Nothing: symbol & {
    __nothing__: boolean;
};
export declare type Key<TValue> = symbol & {
    __value__: TValue;
};
export declare type GetKeyType<TKey> = TKey extends Key<infer T> ? T : never;
export declare function createKey<TValue>(name: string): Key<TValue>;
export declare function useKey<TKey extends Key<any>>(key: TKey, defaultValue?: GetKeyType<TKey> | typeof Nothing): GetKeyType<TKey>;
