import { Key, GetKeyType } from './key';
export interface Provider<TKey extends Key<unknown> = Key<unknown>> {
    key: TKey;
    provide: GetKeyType<TKey>;
}
export declare function provide<TKey extends Key<unknown>>(key: TKey, provide: GetKeyType<TKey>): Provider<TKey>;
export declare function isProvider(thing: any): thing is Provider<Key<unknown>>;
