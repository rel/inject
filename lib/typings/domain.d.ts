import { Key } from './key';
import { Provider } from './providers';
import { Service } from './service';
/** Describes a domain with providers. */
export interface Domain {
    readonly providers: readonly Provider<Key<unknown>>[];
    readonly services?: readonly (Service | Provider<Key<Service>>)[];
}
/** Describes an internal representation of a domain. This uses a cache to speed up lookup times. */
export interface InternalDomain extends Domain {
    readonly $$: Map<Key<unknown>, Provider<Key<unknown>>>;
}
/** The current domain set for this execution context. */
export declare let currentDomain: InternalDomain | void;
/** Asserts this execution context is not inside a domain. */
export declare function assertNotInDomain(message?: string): void;
/** Asserts this execution contect is inside a domain. */
export declare function assertInDomain(message?: string): void;
/** Runs a function inside the execution context of a domain. */
export declare function runInDomain(baseDomain: Domain, callback: (domain: Domain) => void): void;
