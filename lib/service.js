"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var key_1 = require("./key");
var domain_1 = require("./domain");
exports.serviceKeyMap = new WeakMap();
function createService(service) {
    assertServiceIsNotStamped(service);
    exports.serviceKeyMap.set(service, key_1.createKey('GenericServiceKey'));
    // For now...
    return service;
}
exports.createService = createService;
function assertServiceIsStamped(service, message) {
    if (!exports.serviceKeyMap.has(service)) {
        throw new Error(message || 'Did you forget to wrap this service in `createService`?');
    }
}
exports.assertServiceIsStamped = assertServiceIsStamped;
function assertServiceIsNotStamped(service, message) {
    if (exports.serviceKeyMap.has(service)) {
        throw new Error(message || "This service has already been registered!");
    }
}
exports.assertServiceIsNotStamped = assertServiceIsNotStamped;
function provideService(service, override) {
    assertServiceIsStamped(service);
    var key = exports.serviceKeyMap.get(service);
    return {
        key: key,
        provide: override
    };
}
exports.provideService = provideService;
function useService(service) {
    var _a;
    domain_1.assertInDomain();
    assertServiceIsStamped(service);
    var domain = domain_1.currentDomain;
    var key = exports.serviceKeyMap.get(service);
    var override = (_a = domain.$$.get(key)) === null || _a === void 0 ? void 0 : _a.provide;
    return override !== null && override !== void 0 ? override : service;
}
exports.useService = useService;
