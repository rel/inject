"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var utilities_1 = require("./utilities");
function provide(key, provide) {
    return { key: key, provide: provide };
}
exports.provide = provide;
function isProvider(thing) {
    return typeof thing === 'object' && typeof thing.key === 'symbol' && utilities_1.hasOwnProperty(thing, 'provide');
}
exports.isProvider = isProvider;
