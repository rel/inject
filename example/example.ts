import { Domain, runInDomain, createKey, useKey, provide, useService } from '@resynth1943/inject';
import { LoggerService } from './logger';

export const $Console = createKey<Console>('console');
export const $Message = createKey<string>('message');

const appDomain: Domain = {
    providers: [
        provide($Console, console),
        provide($Message, 'Hello, world!')
    ]
};

runInDomain(appDomain, () => {
    const logger = useService(LoggerService);
    logger.log('hello, world!');

    const message = useKey($Message);
    
    if (typeof document === 'object') {
        document.body.innerHTML = message;
    } else {
        logger.log(message);
    }
});
