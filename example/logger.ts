import { useKey, createService } from '@resynth1943/inject';
import { $Console } from './example';

function log (message: string) {
    const console = useKey($Console);
    console.log(message);
}

export const LoggerService = createService({
    log
});
