"use strict";
exports.__esModule = true;
var inject_1 = require("@resynth1943/inject");
var example_1 = require("./example");
function log(message) {
    var console = inject_1.useKey(example_1.$Console);
    console.log(message);
}
exports.LoggerService = inject_1.createService({
    log: log
});
